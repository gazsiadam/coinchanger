package org.gadam.virgo.coins.coinchange.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.gadam.virgo.coins.coinchange.exception.NoDefaultCurrencySetException;
import org.gadam.virgo.coins.coinchange.model.Coin;
import org.gadam.virgo.coins.coinchange.model.Currency;
import org.gadam.virgo.coins.coinchange.model.dto.CoinDTO;
import org.gadam.virgo.coins.coinchange.repo.CoinRepository;
import org.gadam.virgo.coins.coinchange.repo.CurrencyRepository;
import org.gadam.virgo.coins.coinchange.spring.JUnitSpringConfig;
import org.gadam.virgo.coins.coinchange.util.ServerProperty;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { JUnitSpringConfig.class })
@SpringBootApplication
@WebAppConfiguration
@DirtiesContext
public class CoinServiceTest {

	@Autowired
	private CoinChangeService changeService;

	@Autowired
	private CurrencyService currencyService;

	@Autowired
	private CoinRepository coinRepo;

	@Autowired
	private CurrencyRepository curencyRepo;

	@Before
	public void before() {
		this.curencyRepo.deleteAll();
		this.coinRepo.deleteAll();
		
		System.clearProperty(ServerProperty.DEFAULT_CURRENCY.getPropertyName());
	}

	@Test
	public void testAutowireCapabilityForJUnit() {
		assertNotNull(this.changeService);
	}

	@Test(expected = NoDefaultCurrencySetException.class)
	public void testGetChangeNoCurrency() {
		this.changeService.getChange(70);
	}

	@Test
	public void testGetChangeForSpecifiedCurrency() {
		Coin coin = new Coin(1, 1);
		Currency c = new Currency("usd", Stream.of(coin).collect(Collectors.toList()));
		this.currencyService.registerCurrency(c);

		List<CoinDTO> change = this.changeService.getChange(1, "usd");
		assertEquals(1, change.size());
	}

	@Test
	public void testGetChangeForDefaultCurrency() {
		Coin coin = new Coin(1, 1);
		Currency c = new Currency("usd", Stream.of(coin).collect(Collectors.toList()));
		this.currencyService.registerCurrency(c);

		System.setProperty(ServerProperty.DEFAULT_CURRENCY.getPropertyName(), "usd");

		List<CoinDTO> change = this.changeService.getChange(1);
		assertEquals(1, change.size());
	}

}
