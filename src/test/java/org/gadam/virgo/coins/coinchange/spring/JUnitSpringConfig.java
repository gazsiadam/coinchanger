package org.gadam.virgo.coins.coinchange.spring;

import org.gadam.virgo.coins.coinchange.service.algorithm.CoinChanger;
import org.gadam.virgo.coins.coinchange.service.algorithm.greedy.GreedyCoinChanger;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableWebSecurity
@EnableTransactionManagement
@PropertySource("classpath:application.properties")
@EntityScan(basePackages = "org.gadam.virgo.coins.coinchange.model")
@EnableJpaRepositories(basePackages = "org.gadam.virgo.coins.coinchange.repo")

@ComponentScan(basePackages = { "org.gadam.virgo.coins.coinchange" }, //
		excludeFilters = @ComponentScan.Filter(type = FilterType.ASPECTJ, pattern = "org.gadam.virgo.coins.coinchange.spring.*"))

public class JUnitSpringConfig {

	@Bean
	public CoinChanger coinChanger() {
		return new GreedyCoinChanger();
	}
}
