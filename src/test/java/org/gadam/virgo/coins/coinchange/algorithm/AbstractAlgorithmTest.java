package org.gadam.virgo.coins.coinchange.algorithm;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.gadam.virgo.coins.coinchange.exception.CoinChangerException;
import org.gadam.virgo.coins.coinchange.model.Coin;
import org.gadam.virgo.coins.coinchange.service.algorithm.CoinChanger;
import org.junit.Test;

public abstract class AbstractAlgorithmTest {

	protected abstract CoinChanger getCoinChanger();

	@Test
	public void testAlgorithmSameCoin70() {
		CoinChanger coinChanger = getCoinChanger();

		List<Coin> coins = new ArrayList<>();
		coins.add(new Coin(20, 1));
		coins.add(new Coin(35, 2));
		coins.add(new Coin(50, 1));

		Map<Integer, Integer> change = coinChanger.getChange(coins, 70);

		assertEquals(1, change.size());
		Entry<Integer, Integer> solution = change.entrySet().stream().findFirst().orElse(null);
		assertEquals(new Integer(35), solution.getKey());
		assertEquals(new Integer(2), solution.getValue());
	}

	@Test
	public void testAlgorithmSameCoin40() {
		CoinChanger coinChanger = getCoinChanger();

		List<Coin> coins = new ArrayList<>();
		coins.add(new Coin(5, 1));
		coins.add(new Coin(10, 1));
		coins.add(new Coin(20, 2));

		Map<Integer, Integer> change = coinChanger.getChange(coins, 40);

		assertEquals(1, change.size());
		Entry<Integer, Integer> solution = change.entrySet().stream().findFirst().orElse(null);
		assertEquals(new Integer(20), solution.getKey());
		assertEquals(new Integer(2), solution.getValue());
	}

	@Test(expected = CoinChangerException.class)
	public void testAlgorithmValue0() {
		CoinChanger coinChanger = getCoinChanger();

		List<Coin> coins = new ArrayList<>();
		coins.add(new Coin(5, 1));
		coins.add(new Coin(10, 1));
		coins.add(new Coin(20, 2));

		Map<Integer, Integer> change = coinChanger.getChange(coins, 0);

		assertEquals(0, change.size());
	}

	@Test(expected = CoinChangerException.class)
	public void testAlgorithmNoCoinAvailableValue4() {
		CoinChanger coinChanger = getCoinChanger();

		List<Coin> coins = new ArrayList<>();
		coins.add(new Coin(5, 1));
		coins.add(new Coin(10, 1));
		coins.add(new Coin(20, 2));

		coinChanger.getChange(coins, 4);
	}

}
