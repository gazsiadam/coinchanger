package org.gadam.virgo.coins.coinchange.algorithm;

import org.gadam.virgo.coins.coinchange.service.algorithm.CoinChanger;
import org.gadam.virgo.coins.coinchange.service.algorithm.greedy.GreedyCoinChanger;

public class GreedyCoinChangeAlgorithmTest extends AbstractAlgorithmTest {

	protected CoinChanger getCoinChanger() {
		return new GreedyCoinChanger();
	}
}
