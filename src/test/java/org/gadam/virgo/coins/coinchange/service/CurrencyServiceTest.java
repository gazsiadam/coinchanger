package org.gadam.virgo.coins.coinchange.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.gadam.virgo.coins.coinchange.exception.CurrencyAlreadyExistsException;
import org.gadam.virgo.coins.coinchange.exception.CurrencyDoesNotExistException;
import org.gadam.virgo.coins.coinchange.exception.NoDefaultCurrencySetException;
import org.gadam.virgo.coins.coinchange.model.Coin;
import org.gadam.virgo.coins.coinchange.model.Currency;
import org.gadam.virgo.coins.coinchange.repo.CoinRepository;
import org.gadam.virgo.coins.coinchange.repo.CurrencyRepository;
import org.gadam.virgo.coins.coinchange.spring.JUnitSpringConfig;
import org.gadam.virgo.coins.coinchange.util.ServerProperty;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { JUnitSpringConfig.class })
@SpringBootApplication
@WebAppConfiguration
@DirtiesContext
public class CurrencyServiceTest {

	private static final String USD = "usd";

	@Autowired
	private CurrencyService currencyService;

	@Autowired
	private CoinRepository coinRepo;

	@Autowired
	private CurrencyRepository curencyRepo;

	@Before
	public void before() {
		this.curencyRepo.deleteAll();
		this.coinRepo.deleteAll();
	}

	@After
	public void after() {
		System.clearProperty(ServerProperty.DEFAULT_CURRENCY.getPropertyName());
	}

	@Test
	public void testAutowireCapabilityForJUnit() {
		assertNotNull(this.currencyService);
	}

	@Test
	public void testRegisterCurrency() {
		Coin coin = new Coin(1, 1);
		Currency c = new Currency(USD, Stream.of(coin).collect(Collectors.toList()));
		this.currencyService.registerCurrency(c);

		Currency usdCurrency = this.curencyRepo.findByName(USD);
		assertNotNull(usdCurrency);
		assertEquals(USD, usdCurrency.getName());
	}

	@Test(expected = CurrencyAlreadyExistsException.class)
	public void testRegisterDuplicateCurrency() {
		Coin coin = new Coin(1, 1);
		Currency c = new Currency(USD, Stream.of(coin).collect(Collectors.toList()));
		Currency c2 = new Currency(USD, Stream.of(coin).collect(Collectors.toList()));

		this.currencyService.registerCurrency(c);
		this.currencyService.registerCurrency(c2);
	}

	@Test
	public void testCoinStockForCurrency() {
		Coin coin = new Coin(1, 2);
		Currency c = new Currency(USD, Stream.of(coin).collect(Collectors.toList()));
		this.currencyService.registerCurrency(c);

		List<Coin> coinStockForCurrency = this.currencyService.getCoinStockForCurrency(USD);
		assertEquals(1, coinStockForCurrency.size());
		assertEquals(new Integer(1), coinStockForCurrency.get(0).getValue());
		assertEquals(new Integer(2), coinStockForCurrency.get(0).getAmount());
	}

	@Test(expected = CurrencyDoesNotExistException.class)
	public void testCoinStockCurrencyDoesNotExists() {
		this.currencyService.getCoinStockForCurrency(USD);
	}

	@Test
	public void testGetDefaultCurencyName() {
		System.setProperty(ServerProperty.DEFAULT_CURRENCY.getPropertyName(), USD);

		String defaultCurrencyName = this.currencyService.getDefaultCurrencyName();
		assertEquals(USD, defaultCurrencyName);
	}

	@Test(expected = NoDefaultCurrencySetException.class)
	public void testGetDefaultCurencyNameNoneSpecified() {
		this.currencyService.getDefaultCurrencyName();
	}

	@Test
	public void testCurrencyWhichExists() {
		Coin coin = new Coin(1, 2);
		Currency c = new Currency(USD, Stream.of(coin).collect(Collectors.toList()));
		this.currencyService.registerCurrency(c);

		Currency currency = this.currencyService.getCurrency(USD);
		assertEquals(USD, currency.getName());
		assertEquals(1, currency.getCoins().size());
		assertEquals(new Integer(1), currency.getCoins().get(0).getValue());
		assertEquals(new Integer(2), currency.getCoins().get(0).getAmount());
	}

	@Test(expected = CurrencyDoesNotExistException.class)
	public void testCurrencyWhichDoesNotExists() {
		this.currencyService.getCurrency(USD);
	}

}
