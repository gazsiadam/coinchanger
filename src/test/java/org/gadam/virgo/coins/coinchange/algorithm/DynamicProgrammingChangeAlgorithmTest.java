package org.gadam.virgo.coins.coinchange.algorithm;

import org.gadam.virgo.coins.coinchange.service.algorithm.CoinChanger;
import org.gadam.virgo.coins.coinchange.service.algorithm.dynamicp.DinamycProgrammingCoinChanger;

public class DynamicProgrammingChangeAlgorithmTest extends AbstractAlgorithmTest {

	protected CoinChanger getCoinChanger() {
		return new DinamycProgrammingCoinChanger();
	}

}
