package org.gadam.virgo.coins.coinchange.rest;

import static org.junit.Assert.assertEquals;

import java.util.Collections;
import java.util.List;

import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response;

import org.gadam.virgo.coins.coinchange.model.Coin;
import org.gadam.virgo.coins.coinchange.model.Currency;
import org.gadam.virgo.coins.coinchange.rest.gson.IdExclusionStrategy;
import org.gadam.virgo.coins.coinchange.rest.stock.StockRestServlet;
import org.gadam.virgo.coins.coinchange.service.CurrencyService;
import org.gadam.virgo.coins.coinchange.spring.JUnitSpringConfig;
import org.gadam.virgo.coins.coinchange.util.ServerProperty;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class, classes = { JUnitSpringConfig.class })
@WebAppConfiguration
public class StockRestApiTest extends JerseyTest {

	private static final String USD = "usd";
	private static final String HUF = "huf";
	private Currency currency;

	@Autowired
	private CurrencyService currencyService;

	@Override
	protected Application configure() {
		return new ResourceConfig().register(StockRestServlet.class);
	}

	@Before
	public void before() {
		System.setProperty(ServerProperty.DEFAULT_CURRENCY.getPropertyName(), USD);

		List<Coin> emptyList = Collections.emptyList();
		currency = new Currency(USD, emptyList);
		currencyService.registerCurrency(this.currency);
	}

	@Test
	public void testGetStockOfDefaultCurrency() {
		Response response = target("stock/stock").request().get();
		String responseJson = response.readEntity(String.class);

		Gson gson = new GsonBuilder().setPrettyPrinting().setExclusionStrategies(new IdExclusionStrategy()).create();
		String expectedJson = gson.toJson(this.currency);

		assertEquals(expectedJson, responseJson);
	}

	@Test
	public void testGetStockOfNonDefaultCurrency() {
		setupHuf();

		Response response = target("stock/stock").queryParam("currency", HUF).request().get();
		String responseJson = response.readEntity(String.class);

		Gson gson = new GsonBuilder().setPrettyPrinting().setExclusionStrategies(new IdExclusionStrategy()).create();
		String expectedJson = gson.toJson(this.currency);

		assertEquals(expectedJson, responseJson);
	}

	private void setupHuf() {
		List<Coin> emptyList = Collections.emptyList();
		currency = new Currency(HUF, emptyList);
		currencyService.registerCurrency(this.currency);
	}

}
