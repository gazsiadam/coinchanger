package org.gadam.virgo.coins.coinchange.spring;

import static org.junit.Assert.assertNotNull;

import org.gadam.virgo.coins.coinchange.service.CoinChangeService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { JUnitSpringConfig.class })
@SpringBootApplication
@WebAppConfiguration
@DirtiesContext
public class SpringConfigTest {

	@Autowired
	CoinChangeService changeService;

	@Test
	public void testAutowireCapabilityForJUnit() {
		assertNotNull(changeService);
	}

}
