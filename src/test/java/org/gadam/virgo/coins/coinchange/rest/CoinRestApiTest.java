package org.gadam.virgo.coins.coinchange.rest;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response;

import org.gadam.virgo.coins.coinchange.exception.CoinChangerException;
import org.gadam.virgo.coins.coinchange.model.Coin;
import org.gadam.virgo.coins.coinchange.model.Currency;
import org.gadam.virgo.coins.coinchange.model.dto.CoinDTO;
import org.gadam.virgo.coins.coinchange.rest.coin.CoinRestServlet;
import org.gadam.virgo.coins.coinchange.rest.gson.IdExclusionStrategy;
import org.gadam.virgo.coins.coinchange.service.CurrencyService;
import org.gadam.virgo.coins.coinchange.spring.JUnitSpringConfig;
import org.gadam.virgo.coins.coinchange.util.ServerProperty;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class, classes = { JUnitSpringConfig.class })
@WebAppConfiguration
public class CoinRestApiTest extends JerseyTest {

	private static final String USD = "usd";
	private Currency currency;

	@Autowired
	private CurrencyService currencyService;

	@Override
	protected Application configure() {
		return new ResourceConfig().register(CoinRestServlet.class).register(CoinChangerException.class);
	}

	@Before
	public void before() {
		System.setProperty(ServerProperty.DEFAULT_CURRENCY.getPropertyName(), USD);

		List<Coin> emptyList = Arrays.asList(new Coin(1, 2));
		currency = new Currency(USD, emptyList);
		currencyService.registerCurrency(this.currency);
	}

	@Test // expect error message
	public void testMissingValueParam() {
		Response response = target("coin/change").request().get();
		String responseJson = response.readEntity(String.class);

		assertEquals("{\"error\":\"parameter is mandatory: value\"}", responseJson);
	}

	@Test // expect error message
	public void testValueParamIs0() {
		Response response = target("coin/change").queryParam("value", 0).request().get();
		String responseJson = response.readEntity(String.class);

		assertEquals("{\"error\":\"Value must be greater than 0\"}", responseJson);
	}

	@Test
	public void testValueParamIs2() {
		Response response = target("coin/change").queryParam("value", 2).request().get();
		String responseJson = response.readEntity(String.class);

		List<CoinDTO> list = Arrays.asList(new CoinDTO(1, 2));

		Gson gson = new GsonBuilder().setPrettyPrinting().setExclusionStrategies(new IdExclusionStrategy()).create();
		String expectedJson = gson.toJson(list);

		assertEquals(expectedJson, responseJson);
	}

	@Test // expect error message
	public void testValueParamIs2NoEnoughCoins() {
		// first request which uses the 2 available coins
		target("coin/change").queryParam("value", 2).request().get();

		// second request which should fail because of coin availability
		Response response = target("coin/change").queryParam("value", 2).request().get();
		String responseJson = response.readEntity(String.class);

		assertEquals("{\"error\":\"Not enough coins\"}", responseJson);
	}

}
