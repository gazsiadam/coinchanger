package org.gadam.virgo.coins.coinchange.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;

import lombok.Getter;

@Entity
public class Currency extends AbstractDomainObject {

	@Getter
	private String name;

	@Getter
	@OneToMany
	private List<Coin> coins;

	public Currency() {
	}

	public Currency(String name, List<Coin> coins) {
		super();
		this.name = name;
		this.coins = coins;
	}

}
