package org.gadam.virgo.coins.coinchange.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

import org.gadam.virgo.coins.coinchange.rest.ErrorMessage;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public abstract class AbstractMappedException extends AbstractException
		implements ExceptionMapper<AbstractMappedException> {

	private static final long serialVersionUID = -2992665191679458336L;

	public AbstractMappedException() {
		// for JAX-RS excaption mapping
	}

	public AbstractMappedException(String message) {
		super(message);
	}

	protected abstract Response.Status getResponseStatus();

	@Override
	public Response toResponse(AbstractMappedException exception) {
		Gson gson = new GsonBuilder().create();
		String response = gson.toJson(new ErrorMessage(exception.getMessage()));

		return Response.status(getResponseStatus()).entity(response).build();
	}

}
