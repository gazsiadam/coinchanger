package org.gadam.virgo.coins.coinchange.repo;

import org.gadam.virgo.coins.coinchange.model.Currency;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface CurrencyRepository extends CrudRepository<Currency, Long> {
	
	@Query
	Currency findByName(String name);

}
