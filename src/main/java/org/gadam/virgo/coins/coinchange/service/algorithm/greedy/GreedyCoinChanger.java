package org.gadam.virgo.coins.coinchange.service.algorithm.greedy;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.gadam.virgo.coins.coinchange.model.Coin;
import org.gadam.virgo.coins.coinchange.service.algorithm.AbstractCoinChanger;
import org.gadam.virgo.coins.coinchange.service.algorithm.AlgorithmImpl;

/**
 * 
 * @author Vijay Kumar : email kumar.vijay@gmail.com Distributed under Apache
 *         2.0 License
 * 
 *         Programatically solves the Coin Change problem. The program computes
 *         the following 1. All possible solutions 2. The optimal solution.
 * 
 *         References: http://condor.depaul.edu/~rjohnson/algorithm/coins.pdf
 *         http://www.algorithmist.com/index.php/Coin_Change
 * 
 */
@AlgorithmImpl
public class GreedyCoinChanger extends AbstractCoinChanger {

	/**
	 * Find the optimal solution for a given target value and the set of
	 * denomiations
	 * 
	 * @param target
	 * @param denoms
	 * @return
	 */
	public GreedyCoinChangeResult findOptimalChange(Integer target, Integer[] denoms) {
		GreedyCoinChangeResult soln = new GreedyCoinChangeResult(target, denoms);
		StringBuilder sb = new StringBuilder();

		// initialize the solution structure
		for (int i = 0; i < soln.OPT[0].length; i++) {
			soln.OPT[0][i] = i;
			soln.optimalChange[0][i] = sb.toString();
			sb.append(denoms[0] + " ");
		}

		// Read through the following for more details on the explanation
		// of the algorithm.
		// http://condor.depaul.edu/~rjohnson/algorithm/coins.pdf
		for (int i = 1; i < denoms.length; i++) {
			for (int j = 0; j < target + 1; j++) {
				int value = j;
				int targetWithPrevDenomiation = soln.OPT[i - 1][j];
				int ix = (value) - denoms[i];
				if (ix >= 0 && (denoms[i] <= value)) {
					int x2 = denoms[i] + soln.OPT[i][ix];
					if (x2 <= target && (1 + soln.OPT[i][ix] < targetWithPrevDenomiation)) {
						String temp = soln.optimalChange[i][ix] + denoms[i] + " ";
						soln.optimalChange[i][j] = temp;
						soln.OPT[i][j] = 1 + soln.OPT[i][ix];
					} else {
						soln.optimalChange[i][j] = soln.optimalChange[i - 1][j] + " ";
						soln.OPT[i][j] = targetWithPrevDenomiation;
					}
				} else {
					soln.optimalChange[i][j] = soln.optimalChange[i - 1][j];
					soln.OPT[i][j] = targetWithPrevDenomiation;
				}
			}
		}
		return soln;
	}

	@Override
	public Map<Integer, Integer> calculateChange(List<Coin> coins, Integer value) {
		Integer[] iCoins = coins.stream().map(Coin::getValue).collect(Collectors.toList())
				.toArray(new Integer[coins.size()]);

		GreedyCoinChangeResult soln = this.findOptimalChange(value, iCoins);
		List<Integer> solution = soln.getSolution();

		return fillResultMap(solution);
	}

	private Map<Integer, Integer> fillResultMap(List<Integer> solution) {
		return solution.stream()
				.collect(Collectors.groupingBy(Function.identity(), Collectors.reducing(0, e -> 1, Integer::sum)));
	}

}