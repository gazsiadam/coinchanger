package org.gadam.virgo.coins.coinchange.rest.coin;

import java.util.List;
import java.util.Objects;
import java.util.logging.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import org.gadam.virgo.coins.coinchange.model.dto.CoinDTO;
import org.gadam.virgo.coins.coinchange.rest.AbstractRestServlet;
import org.gadam.virgo.coins.coinchange.rest.gson.IdExclusionStrategy;
import org.gadam.virgo.coins.coinchange.service.CoinChangeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Component
@Path("coin")
public class CoinRestServlet extends AbstractRestServlet {

	private static final Logger LOGGER = Logger.getLogger(CoinRestServlet.class.getName());

	@Autowired
	private CoinChangeService coinService;

	@GET
	@Produces("application/json")
	@Path("/change")
	public String getChange(@QueryParam("value") Integer value, @QueryParam("currency") String currency) {
		LOGGER.info("coin change request recevied with amount: " + value);

		if (Objects.isNull(value)) {
			throwErrorMessage("parameter is mandatory: value");
		}

		List<CoinDTO> change = coinService.getChange(value);

		Gson gson = new GsonBuilder().setExclusionStrategies(new IdExclusionStrategy()).setPrettyPrinting().create();
		return gson.toJson(change);
	}

}
