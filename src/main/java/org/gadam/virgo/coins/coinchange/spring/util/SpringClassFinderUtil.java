package org.gadam.virgo.coins.coinchange.spring.util;

import java.lang.annotation.Annotation;
import java.util.Optional;
import java.util.Set;

import org.gadam.virgo.coins.coinchange.service.algorithm.CoinChanger;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AnnotationTypeFilter;

public class SpringClassFinderUtil {

	public static Object findClassByAnnotation(Class<? extends Annotation> annotationClass, String implName, String basePackage)
			throws InstantiationException, IllegalAccessException, ClassNotFoundException {
		
		ClassPathScanningCandidateComponentProvider scanner = new ClassPathScanningCandidateComponentProvider(true);
		scanner.addIncludeFilter(new AnnotationTypeFilter(annotationClass));
		Set<BeanDefinition> candidates = scanner
				.findCandidateComponents(basePackage);

		Optional<BeanDefinition> candidate = candidates.stream().filter(a -> a.getBeanClassName().equals(implName))
				.findFirst();

		if (candidate.isPresent()) {
			return (CoinChanger) Class.forName(candidate.get().getBeanClassName()).newInstance();
		}

		return null;
	}
}