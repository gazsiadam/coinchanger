package org.gadam.virgo.coins.coinchange.exception;

public class MandatoryPropertyNotSetException extends AbstractException {

	public MandatoryPropertyNotSetException(String propertyName) {
		super(propertyName);
	}

	private static final long serialVersionUID = 6733654083782499700L;
	
}
