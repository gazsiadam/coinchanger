package org.gadam.virgo.coins.coinchange.service.impl;

import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.gadam.virgo.coins.coinchange.exception.CoinChangerException;
import org.gadam.virgo.coins.coinchange.model.Currency;
import org.gadam.virgo.coins.coinchange.model.dto.CoinDTO;
import org.gadam.virgo.coins.coinchange.rest.coin.CoinRestServlet;
import org.gadam.virgo.coins.coinchange.service.CoinChangeService;
import org.gadam.virgo.coins.coinchange.service.CurrencyService;
import org.gadam.virgo.coins.coinchange.service.algorithm.CoinChanger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CoinChangeServiceImpl implements CoinChangeService {

	private static final Logger LOGGER = Logger.getLogger(CoinRestServlet.class.getName());

	@Autowired
	private CoinChanger coinChanger;

	@Autowired
	private CurrencyService currencyService;

	@Override
	public List<CoinDTO> getChange(Integer value) {
		String defaultCurrencyName = this.currencyService.getDefaultCurrencyName();
		return this.getChange(value, defaultCurrencyName);
	}

	@Override
	public List<CoinDTO> getChange(Integer value, String currencyName) {
		Currency currency = this.currencyService.getCurrency(currencyName);

		try {
			Map<Integer, Integer> change = coinChanger.getChange(currency.getCoins(), value);
			List<CoinDTO> result = change.entrySet().stream().map(e -> new CoinDTO(e.getKey(), e.getValue()))
					.collect(Collectors.toList());

			this.currencyService.updateCurrencyElements(currencyName, result);

			return result;
		} catch (Exception e) {
			LOGGER.warning(e.getClass().getName() + ": " + e.getMessage());
			throw new CoinChangerException(e.getMessage());
		}
	}

}