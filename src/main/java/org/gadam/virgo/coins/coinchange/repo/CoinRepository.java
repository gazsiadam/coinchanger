package org.gadam.virgo.coins.coinchange.repo;

import org.gadam.virgo.coins.coinchange.model.Coin;
import org.springframework.data.repository.CrudRepository;

public interface CoinRepository extends CrudRepository<Coin, Long> {
	
}
