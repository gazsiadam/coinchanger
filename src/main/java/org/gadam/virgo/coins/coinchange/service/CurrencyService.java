package org.gadam.virgo.coins.coinchange.service;

import java.util.List;

import org.gadam.virgo.coins.coinchange.model.Coin;
import org.gadam.virgo.coins.coinchange.model.Currency;
import org.gadam.virgo.coins.coinchange.model.dto.CoinDTO;

public interface CurrencyService {

	/**
	 * Register a new currency
	 * 
	 * @param currency
	 */
	void registerCurrency(Currency currency);

	/**
	 * Get the stock of all coins for a given currency to see how many coins
	 * there are in the cash register. If no currency is specified, then use the
	 * default one
	 * 
	 * @param currency
	 * @return a list of pair (coin value, amount of coins) from the cash
	 *         register
	 */
	List<Coin> getCoinStockForCurrency(String currency);

	/**
	 * Get the default currency name
	 * 
	 * @return default currency's name if exists
	 */
	String getDefaultCurrencyName();

	/**
	 * Get the currency with the given name
	 * 
	 * @param currencyName
	 * @return
	 */
	Currency getCurrency(String currencyName);

	/**
	 * Update the number of available coin numbers for the given currency
	 * 
	 * @param currencyName
	 * @param result
	 */
	void updateCurrencyElements(String currencyName, List<CoinDTO> result);

}
