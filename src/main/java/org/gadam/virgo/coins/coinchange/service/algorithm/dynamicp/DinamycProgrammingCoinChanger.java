package org.gadam.virgo.coins.coinchange.service.algorithm.dynamicp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.gadam.virgo.coins.coinchange.model.Coin;
import org.gadam.virgo.coins.coinchange.service.algorithm.AbstractCoinChanger;
import org.gadam.virgo.coins.coinchange.service.algorithm.AlgorithmImpl;

/**
 * This solution was found on github.
 * It uses dynamic programming to solve the coin change problem.
 * 
 * https://github.com/mission-peace/interview/blob/master/src/com/interview/dynamic/CoinChangingMinimumCoin.java
 */

@AlgorithmImpl
public class DinamycProgrammingCoinChanger extends AbstractCoinChanger {

	/**
	 * Bottom up way of solving this problem. Keep input sorted. Otherwise temp[j-arr[i]) + 1 can become Integer.Max_value + 1 which can be very low negative
	 * number Returns Integer.MAX_VALUE - 1 if solution is not possible.
	 */
	public int[] minimumCoinBottomUp(int total, Integer[] iCoins) {
		int T[] = new int[total + 1];
		int R[] = new int[total + 1];
		T[0] = 0;
		for (int i = 1; i <= total; i++) {
			T[i] = Integer.MAX_VALUE - 1;
			R[i] = -1;
		}
		for (int j = 0; j < iCoins.length; j++) {
			for (int i = 1; i <= total; i++) {
				if (i >= iCoins[j]) {
					if (T[i - iCoins[j]] + 1 < T[i]) {
						T[i] = 1 + T[i - iCoins[j]];
						R[i] = j;
					}
				}
			}
		}
		return R;
	}

	private List<Integer> printCoinCombination(int R[], Integer[] iCoins) {
		if (R[R.length - 1] == -1) {
			return Collections.emptyList();
		}
		
		List<Integer> list = new ArrayList<>();
		
		int start = R.length - 1;
		while (start != 0) {
			int j = R[start];
			// System.out.print(coins[j] + " ");
			list.add(iCoins[j]);
			start = start - iCoins[j];
		}
		
		return list;
	}

	@Override
	protected Map<Integer, Integer> calculateChange(List<Coin> coins, Integer value) {
		Integer[] iCoins = coins.stream().map(Coin::getValue).collect(Collectors.toList())
				.toArray(new Integer[coins.size()]);

		int[] changedCoinsList = minimumCoinBottomUp(value, iCoins);
		List<Integer> solution = printCoinCombination(changedCoinsList, iCoins);
		
		return fillResultMap(solution);
	}
	
	private Map<Integer, Integer> fillResultMap(List<Integer> solution) {
		return solution.stream()
				.collect(Collectors.groupingBy(Function.identity(), Collectors.reducing(0, e -> 1, Integer::sum)));
	}

}
