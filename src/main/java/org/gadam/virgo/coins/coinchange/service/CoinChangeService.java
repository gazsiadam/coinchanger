package org.gadam.virgo.coins.coinchange.service;

import java.util.List;

import org.gadam.virgo.coins.coinchange.model.dto.CoinDTO;

public interface CoinChangeService {

	/**
	 * Calculate the necessary coins to get an even change for the given amount
	 * The default currency will be used
	 * 
	 * @param value
	 * @return return a map which holds the value of the coin and how many is
	 *         needed
	 */
	List<CoinDTO> getChange(Integer value);

	/**
	 * Calculate the necessary coins to get an even change for the given amount
	 * 
	 * @param value
	 * @param currency
	 * @return return a map which holds the value of the coin and how many is
	 *         needed
	 */
	List<CoinDTO> getChange(Integer value, String currency);

}
