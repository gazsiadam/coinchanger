package org.gadam.virgo.coins.coinchange.service.impl;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.gadam.virgo.coins.coinchange.exception.CoinChangerException;
import org.gadam.virgo.coins.coinchange.exception.CurrencyAlreadyExistsException;
import org.gadam.virgo.coins.coinchange.exception.CurrencyDoesNotExistException;
import org.gadam.virgo.coins.coinchange.exception.NoDefaultCurrencySetException;
import org.gadam.virgo.coins.coinchange.model.Coin;
import org.gadam.virgo.coins.coinchange.model.Currency;
import org.gadam.virgo.coins.coinchange.model.dto.CoinDTO;
import org.gadam.virgo.coins.coinchange.repo.CoinRepository;
import org.gadam.virgo.coins.coinchange.repo.CurrencyRepository;
import org.gadam.virgo.coins.coinchange.service.CurrencyService;
import org.gadam.virgo.coins.coinchange.util.ServerProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CurrencyServiceImpl implements CurrencyService {

	@Autowired
	private CurrencyRepository currencyRepo;

	@Autowired
	private CoinRepository coinRepo;

	@Override
	public void registerCurrency(Currency currency) {
		if (Objects.nonNull(this.currencyRepo.findByName(currency.getName()))) {
			throw new CurrencyAlreadyExistsException(currency.getName());
		}

		currency.getCoins().stream().forEach(coinRepo::save);
		this.currencyRepo.save(currency);
	}

	@Override
	public List<Coin> getCoinStockForCurrency(String currencyName) {
		if (Objects.isNull(currencyName)) {
			return getCurrency(getDefaultCurrencyName()).getCoins();
		}

		return getCurrency(currencyName).getCoins();
	}

	@Override
	public String getDefaultCurrencyName() {
		String currencyName = ServerProperty.DEFAULT_CURRENCY.getStringProperty();

		if (Objects.isNull(currencyName)) {
			throw new NoDefaultCurrencySetException();
		}

		return currencyName;
	}

	@Override
	public Currency getCurrency(String currencyName) {
		Currency currency = currencyRepo.findByName(currencyName);

		if (Objects.isNull(currency)) {
			throw new CurrencyDoesNotExistException(currencyName);
		}

		return currency;
	}

	@Override
	public void updateCurrencyElements(String currencyName, List<CoinDTO> result) {
		Currency currency = this.getCurrency(currencyName);

		for (CoinDTO coinDTO : result) {
			Optional<Coin> coin = currency.getCoins().stream().filter(a -> a.getValue().equals(coinDTO.getValue()))
					.findFirst();

			updateCoinAvailability(coinDTO, coin);
		}
	}

	private void updateCoinAvailability(CoinDTO coinDTO, Optional<Coin> coin) {
		if (!coin.isPresent()) {
			throw new CoinChangerException("Cannot update coin availability. Coin not found");
		}

		Coin updateableCoin = coin.get();

		int substraction = Math.subtractExact(updateableCoin.getAmount(), coinDTO.getAmount());
		if (substraction < 0) {
			throw new CoinChangerException("Not enough coins");
		}

		updateableCoin.setAmount(substraction);
		this.coinRepo.save(updateableCoin);
	}

}
