package org.gadam.virgo.coins.coinchange.rest.stock;

import java.util.Objects;
import java.util.logging.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.xml.ws.handler.MessageContext;

import org.gadam.virgo.coins.coinchange.model.Currency;
import org.gadam.virgo.coins.coinchange.rest.AbstractRestServlet;
import org.gadam.virgo.coins.coinchange.rest.coin.CoinRestServlet;
import org.gadam.virgo.coins.coinchange.rest.gson.IdExclusionStrategy;
import org.gadam.virgo.coins.coinchange.service.CurrencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Component
@Path("/stock")
public class StockRestServlet extends AbstractRestServlet {

	private static final Logger LOGGER = Logger.getLogger(CoinRestServlet.class.getName());

	@Context
	private MessageContext mc;

	@Autowired
	private CurrencyService service;

	@GET
	@Produces("application/json")
	@Path("/stock")
	public String getStock(@QueryParam("currency") String currency) throws NoSuchFieldException, SecurityException {
		LOGGER.info("stock request recevied for currency: " + currency);

		if (Objects.isNull(currency)) {
			currency = this.service.getDefaultCurrencyName();
		}
		Currency change = this.service.getCurrency(currency);

		Gson gson = new GsonBuilder().setExclusionStrategies(new IdExclusionStrategy()).setPrettyPrinting().create();
		return gson.toJson(change);
	}

}
