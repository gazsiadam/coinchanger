package org.gadam.virgo.coins.coinchange.rest;

import javax.ws.rs.ApplicationPath;

import org.gadam.virgo.coins.coinchange.exception.CoinChangerException;
import org.gadam.virgo.coins.coinchange.exception.CurrencyDoesNotExistException;
import org.gadam.virgo.coins.coinchange.exception.NoDefaultCurrencySetException;
import org.gadam.virgo.coins.coinchange.rest.coin.CoinRestServlet;
import org.gadam.virgo.coins.coinchange.rest.stock.StockRestServlet;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

@Component
@ApplicationPath("/rest")
public class RestApiApplicationConfiguration extends ResourceConfig {

	public RestApiApplicationConfiguration() {

		// servlets
		register(CoinRestServlet.class);
		register(StockRestServlet.class);

		// mapped exceptions
		register(NoDefaultCurrencySetException.class);
		register(CurrencyDoesNotExistException.class);
		register(CoinChangerException.class);
	}
}
