package org.gadam.virgo.coins.coinchange.spring;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.gadam.virgo.coins.coinchange.exception.MandatoryPropertyNotSetException;
import org.gadam.virgo.coins.coinchange.exception.WrongNumberOfCurrencyCoinsException;
import org.gadam.virgo.coins.coinchange.model.Coin;
import org.gadam.virgo.coins.coinchange.model.Currency;
import org.gadam.virgo.coins.coinchange.service.CurrencyService;
import org.gadam.virgo.coins.coinchange.service.algorithm.AlgorithmImpl;
import org.gadam.virgo.coins.coinchange.service.algorithm.CoinChanger;
import org.gadam.virgo.coins.coinchange.service.algorithm.dynamicp.DinamycProgrammingCoinChanger;
import org.gadam.virgo.coins.coinchange.spring.security.CustomBasicAuthenticationEntryPoint;
import org.gadam.virgo.coins.coinchange.spring.util.SpringClassFinderUtil;
import org.gadam.virgo.coins.coinchange.util.PropertyUtil;
import org.gadam.virgo.coins.coinchange.util.ServerProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
@EnableWebSecurity
@EnableTransactionManagement
@PropertySource("classpath:application.properties")
@ComponentScan(basePackages = { "org.gadam.virgo.coins.coinchange" })
@EntityScan(basePackages = "org.gadam.virgo.coins.coinchange.model")
@EnableJpaRepositories(basePackages = "org.gadam.virgo.coins.coinchange.repo")
public class AppConfig extends WebSecurityConfigurerAdapter {

	private static final String SECURITY_USER_NAME = "security.user.name";
	private static final String SECURITY_USER_PASSWORD = "security.user.password";
	private static final String MANAGEMENT_SECURITY_ROLES = "management.security.roles";

	private static final String CURRENCIES = "currencies";
	private static final String CURRENCY_COINS_PREFIX = "currency.coins.";
	private static final String CURRENCY_COINS_AMOUNT_PREFIX = "currency.coins.amount.";

	@Resource
	private Environment env;

	@Autowired
	private CurrencyService service;

	@PostConstruct
	public void loadProperties() {
		// load server properties
		Stream.of(ServerProperty.values()).forEach(p -> loadServerProperty(p));

		// load currencies
		String currenciesProp = env.getRequiredProperty(CURRENCIES);
		List<String> currencyNames = PropertyUtil.getStringArray(currenciesProp, " ");
		currencyNames.stream().map(this::readCurencysCoins).forEach(this.service::registerCurrency);
	}

	private void loadServerProperty(ServerProperty p) {
		String property = this.env.getProperty(p.getPropertyName());

		
		if (property == null) {
			if (p.getMandatory()) {
				throw new MandatoryPropertyNotSetException(p.getPropertyName());
			}
			
			return;
		}

		System.setProperty(p.getPropertyName(), property);
	}

	private Currency readCurencysCoins(String currencyName) {
		String coinsProp = env.getRequiredProperty(CURRENCY_COINS_PREFIX + currencyName.toLowerCase());
		String amountProp = env.getRequiredProperty(CURRENCY_COINS_AMOUNT_PREFIX + currencyName.toLowerCase());

		List<Integer> coins = PropertyUtil.getIntegerList(coinsProp, " ");
		List<Integer> amount = PropertyUtil.getIntegerList(amountProp, " ");

		if (coins.size() != new HashSet<>(coins).size()) {
			throw new WrongNumberOfCurrencyCoinsException();
		}

		List<Coin> coinList = IntStream.range(0, Math.min(coins.size(), amount.size())).boxed()
				.map(i -> new Coin(coins.get(i), amount.get(i))).collect(Collectors.toList());

		return new Currency(currencyName, coinList);
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests() //
				.antMatchers("/rest/coin/**").permitAll() // non-secured api
				.antMatchers("/rest/stock/**").hasRole("ADMIN").and().httpBasic().realmName("ADMIN")
				.authenticationEntryPoint(getBasicAuthEntryPoint()).and().sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.STATELESS);
	}

	@Bean
	public CustomBasicAuthenticationEntryPoint getBasicAuthEntryPoint() {
		return new CustomBasicAuthenticationEntryPoint();
	}

	@Bean
	public CoinChanger coinChanger() throws InstantiationException, IllegalAccessException, ClassNotFoundException {
		String algorithmName = ServerProperty.DEFAULT_ALGORITHM.getStringProperty();

		Object impl = SpringClassFinderUtil.findClassByAnnotation(AlgorithmImpl.class, algorithmName,
				ServerProperty.DEFAULT_ALGORITHM_PACKAGE.getStringProperty());
		if (Objects.isNull(impl)) {
			return new DinamycProgrammingCoinChanger();
		}

		return (CoinChanger) impl;
	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		String uname = env.getRequiredProperty(SECURITY_USER_NAME);
		String pword = env.getRequiredProperty(SECURITY_USER_PASSWORD);
		String role = env.getRequiredProperty(MANAGEMENT_SECURITY_ROLES);

		auth.inMemoryAuthentication().withUser(uname).password(pword).roles(role);
	}

}
