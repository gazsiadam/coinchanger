package org.gadam.virgo.coins.coinchange.util;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PropertyUtil {

	public static List<String> getStringArray(String propertyValue, String splitter) {
		return Stream.of(propertyValue.split(splitter)).collect(Collectors.toList());
	}
	
	public static List<Integer> getIntegerList(String propertyValue, String splitter) {
		return Stream.of(propertyValue.split(splitter)).map(Integer::parseInt).collect(Collectors.toList());
	}

	public static Set<Integer> getIntegerSet(String propertyValue, String splitter) {
		return Stream.of(propertyValue.split(splitter)).map(Integer::parseInt).collect(Collectors.toSet());
	}

}
