package org.gadam.virgo.coins.coinchange.service.algorithm;

import java.util.List;
import java.util.Map;

import org.gadam.virgo.coins.coinchange.model.Coin;

public interface CoinChanger {

	/**
	 * Calculate the optimal change for the given amount using the given list of
	 * coins
	 * 
	 * @param coins
	 * @param value
	 * @return a map with pairs of (Coin, amount) which says which coin should
	 *         be used how many times
	 */
	Map<Integer, Integer> getChange(List<Coin> coins, Integer value);

}
