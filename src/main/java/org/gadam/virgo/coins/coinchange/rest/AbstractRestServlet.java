package org.gadam.virgo.coins.coinchange.rest;

import java.util.Date;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public abstract class AbstractRestServlet {

	@GET
	@Path("/status")
	public String getStatus() {
		return new Date().toString() + "<br>" + "Rest API running";
	}

	protected void throwErrorMessage(String message) {
		Gson gson = new GsonBuilder().create();
		String response = gson.toJson(new ErrorMessage(message));

		throw new WebApplicationException(Response.status(Response.Status.NOT_FOUND).entity(response).build());
	}

}
