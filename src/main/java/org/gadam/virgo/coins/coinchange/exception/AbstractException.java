package org.gadam.virgo.coins.coinchange.exception;

public abstract class AbstractException extends RuntimeException {

	private static final long serialVersionUID = -2992665191679458336L;

	public AbstractException() {
	}

	public AbstractException(String message) {
		super(message);
	}

}
