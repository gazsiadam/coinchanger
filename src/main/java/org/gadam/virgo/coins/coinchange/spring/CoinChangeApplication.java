package org.gadam.virgo.coins.coinchange.spring;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

@SpringBootApplication
public class CoinChangeApplication implements WebApplicationInitializer {

	public static void main(String[] args) {
		SpringApplication.run(CoinChangeApplication.class, args);
	}

	public void onStartup(ServletContext servletContext) throws ServletException {
		AnnotationConfigWebApplicationContext ctx = new AnnotationConfigWebApplicationContext();
		ctx.register(AppConfig.class);
		servletContext.addListener(new ContextLoaderListener(ctx));
	}
}
