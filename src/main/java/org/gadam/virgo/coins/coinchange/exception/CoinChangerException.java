package org.gadam.virgo.coins.coinchange.exception;

import javax.ws.rs.core.Response.Status;

public class CoinChangerException extends AbstractMappedException {

	public CoinChangerException() {
		// for JAX-RS excaption mapping
	}

	public CoinChangerException(String message) {
		super(message);
	}

	private static final long serialVersionUID = -6451817806688557622L;

	@Override
	protected Status getResponseStatus() {
		return Status.EXPECTATION_FAILED;
	}

}
