package org.gadam.virgo.coins.coinchange.rest.gson;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;

public class IdExclusionStrategy implements ExclusionStrategy {

	@Override
	public boolean shouldSkipField(FieldAttributes f) {
		return (f.getName().equals("id"));
	}

	@Override
	public boolean shouldSkipClass(Class<?> clazz) {
		return false;
	}

}
