package org.gadam.virgo.coins.coinchange.service.algorithm.greedy;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Vijay Kumar : email kumar.vijay@gmail.com Distributed under Apache
 *         2.0 License
 *
 *         The following class contains the final result after running the coin
 *         change function.
 */

public class GreedyCoinChangeResult {
	/**
	 * The optimal solution for i,j where i is the denomination and j is the
	 * current target value. The final optimal solution is the number of coins
	 * required to reach the 'target. The final solution will be the cell with
	 * the highest index eg:- for a target amount of 12 and denomination of
	 * 1,6,10 the highest index will contain the integer value 2 which
	 * corresponds to 2 coins required to make a target of 12 (using 6,6).
	 */
	public Integer OPT[][];
	/**
	 * similar to the OPT structure, except that the elements are strings of
	 * change for the i,j optimal solution eg:- for a target amount of 12 and
	 * denomination of 1,6,10 the highest index will contain the string "6 6".
	 */
	public String optimalChange[][];

	/**
	 * List of all possible solutions for the target
	 */
	public ArrayList<String> allPossibleChanges = new ArrayList<String>();

	/**
	 * Copy of the denominations that was used to generate this solution
	 */
	public Integer[] denoms;

	public GreedyCoinChangeResult(Integer target, Integer[] denoms) {
		this.denoms = denoms;
		optimalChange = new String[denoms.length][target + 1];
		OPT = new Integer[denoms.length][target + 1];
	}

	public List<Integer> getSolution() {
		int i = optimalChange.length;
		int j = optimalChange[0].length;

		return Stream.of(optimalChange[i - 1][j - 1].split(" ")).map(Integer::parseInt).collect(Collectors.toList());
	}

	public String denomString() {
		StringBuilder sb = new StringBuilder();
		for (int i : denoms) {
			sb.append(i + " ");
		}

		return sb.toString();
	}
}
