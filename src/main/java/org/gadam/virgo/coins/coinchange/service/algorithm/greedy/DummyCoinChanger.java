package org.gadam.virgo.coins.coinchange.service.algorithm.greedy;

import java.util.List;
import java.util.Map;

import org.gadam.virgo.coins.coinchange.model.Coin;
import org.gadam.virgo.coins.coinchange.service.algorithm.AbstractCoinChanger;
import org.gadam.virgo.coins.coinchange.service.algorithm.AlgorithmImpl;

@AlgorithmImpl
public class DummyCoinChanger extends AbstractCoinChanger {

	@Override
	public Map<Integer, Integer> calculateChange(List<Coin> coins, Integer value) {
		return null;
	}

}