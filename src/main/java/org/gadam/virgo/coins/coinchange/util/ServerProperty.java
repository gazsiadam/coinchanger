package org.gadam.virgo.coins.coinchange.util;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import lombok.Getter;

public enum ServerProperty {

	DEFAULT_CURRENCY("server.default.currency", false),
	DEFAULT_ALGORITHM("server.default.algorithm", false),
	DEFAULT_ALGORITHM_PACKAGE("server.default.algorithm.base.package", false);

	private String property;
	
	@Getter
	private Boolean mandatory;

	private ServerProperty(String property, Boolean mandatory) {
		this.property = property;
		this.mandatory = mandatory;
	}

	public String getStringProperty() {
		return System.getProperty(property);
	}

	public Boolean getBooleanProperty() {
		String value = System.getProperty(this.property);

		Boolean res = Boolean.FALSE;
		try {
			res = Boolean.parseBoolean(value);
		} catch (Exception e) {
			Logger.getLogger(ServerProperty.class.getName())
					.warning("Convertion of value '" + value + "' from property '" + this.property + "' to '"
							+ Boolean.class + "' failed with error: " + e.getMessage());
		}

		return res;
	}

	public String getPropertyName() {
		return this.property;
	}

	public Set<String> getRolesProperty() {
		String value = System.getProperty(this.property);

		if (value == null) {
			return Collections.emptySet();
		}

		return new HashSet<>(Arrays.stream(value.split(",")).collect(Collectors.toSet()));
	}

}
