package org.gadam.virgo.coins.coinchange.model;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import lombok.Getter;

@Getter
@MappedSuperclass
public abstract class AbstractDomainObject {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

}
