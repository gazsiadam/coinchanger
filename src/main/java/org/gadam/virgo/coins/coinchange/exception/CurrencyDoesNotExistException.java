package org.gadam.virgo.coins.coinchange.exception;

import javax.ws.rs.core.Response.Status;

public class CurrencyDoesNotExistException extends AbstractMappedException {

	private static final long serialVersionUID = -6451817806688557622L;

	public CurrencyDoesNotExistException() {
		// for JAX-RS excaption mapping
	}

	public CurrencyDoesNotExistException(String currency) {
		super(currency);
	}

	@Override
	protected Status getResponseStatus() {
		return Status.NOT_FOUND;
	}

	@Override
	public String getMessage() {
		return "The given currency " + super.getMessage() + " does not exist";
	}

}
