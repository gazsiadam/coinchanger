package org.gadam.virgo.coins.coinchange.exception;

import javax.ws.rs.core.Response.Status;

public class NoDefaultCurrencySetException extends AbstractMappedException {

	private static final long serialVersionUID = -6451817806688557622L;

	public NoDefaultCurrencySetException() {
		// for JAX-RS excaption mapping
	}

	@Override
	protected Status getResponseStatus() {
		return Status.NOT_FOUND;
	}

	@Override
	public String getMessage() {
		return "No default currency has been specified. Specify one in the application's property file using server.default.currency property";
	}

}
