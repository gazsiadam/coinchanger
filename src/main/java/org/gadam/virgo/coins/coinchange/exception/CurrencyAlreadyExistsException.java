package org.gadam.virgo.coins.coinchange.exception;

public class CurrencyAlreadyExistsException extends AbstractException {

	private static final long serialVersionUID = -6451817806688557622L;

	public CurrencyAlreadyExistsException() {
	}

	public CurrencyAlreadyExistsException(String currency) {
		super(currency);
	}

	@Override
	public String getMessage() {
		return "The given currency already exists: " + super.getMessage();
	}

}
