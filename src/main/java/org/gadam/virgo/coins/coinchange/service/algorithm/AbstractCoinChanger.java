package org.gadam.virgo.coins.coinchange.service.algorithm;

import java.util.List;
import java.util.Map;

import org.gadam.virgo.coins.coinchange.exception.CoinChangerException;
import org.gadam.virgo.coins.coinchange.model.Coin;

public abstract class AbstractCoinChanger implements CoinChanger {

	@Override
	public Map<Integer, Integer> getChange(List<Coin> coins, Integer value) {
		validateInput(coins, value);
		return calculateChange(coins, value);
	}

	protected abstract Map<Integer, Integer> calculateChange(List<Coin> coins, Integer value);

	protected void validateInput(List<Coin> coins, Integer value) {
		if (value <= 0) {
			throw new CoinChangerException("Value must be greater than 0");
		}

		if (coins.size() == 0) {
			throw new CoinChangerException("No coins specified for the given currency");
		}

		if (coins.stream().map(Coin::getValue).min(Integer::compare).orElse(Integer.MAX_VALUE).compareTo(value) == 1) {
			throw new CoinChangerException(
					"Value can not be split. The value is lesser as the smallest coin available");
		}
	}

}
