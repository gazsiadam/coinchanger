package org.gadam.virgo.coins.coinchange.model;

import javax.persistence.Entity;

import lombok.Getter;
import lombok.Setter;

@Entity
public class Coin extends AbstractDomainObject {

	@Getter
	private Integer value;

	@Getter
	@Setter
	private Integer amount;

	public Coin() {
	}

	public Coin(Integer value, Integer available) {
		this.value = value;
		this.amount = available;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("{\"value\":");
		sb.append(value);

		sb.append(", \"amount\":");
		sb.append(amount);

		sb.append("}");

		return sb.toString();
	}

}
