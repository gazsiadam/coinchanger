# BootSpring Application
Just run CoinChangeApplication.java

# Two algorithms are available
1. GreedyCoinChanger - works for an input value of around 40.000 (very inefficient memory usage)
2. (default) DinamycProgrammingCoinChanger - works for an input value of around 80.000.000